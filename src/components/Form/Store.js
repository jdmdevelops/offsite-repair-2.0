import React from 'react'
import { useEffect } from 'react'

import { TextField, Typography } from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  input: {
    marginBottom: '10px',
  },
  city: {
    width: '48%',
    marginRight: '2%',
    marginBottom: '10px',
  },
  state: {
    width: '18%',
    marginRight: '2%',
    marginBottom: '10px',
  },
  zip: {
    width: '30%',
    marginBottom: '10px',
  },
})

const Store = () => {
  const classes = useStyles()
  const [state, setState] = React.useState({
    name: '',
    address: '',
    city: '',
    state: '',
    zip: '',
    pointOfContact: '',
  })

  const handleInput = (e) =>
    setState({
      ...state,
      [e.target.name]: e.target.value,
    })

  return (
    <React.Fragment>
      <Typography variant="h4" gutterBottom>
        Store
      </Typography>

      <TextField
        className={classes.input}
        label="Store Name"
        id="store"
        name="name"
        onChange={handleInput}
        value={state.name || ''}
        variant="outlined"
        fullWidth
      />
      <TextField
        className={classes.input}
        label="Address"
        id="store"
        name="address"
        onChange={handleInput}
        value={state.address || ''}
        variant="outlined"
        fullWidth
      />
      <TextField
        className={classes.city}
        label="City"
        id="store"
        name="city"
        onChange={handleInput}
        value={state.city || ''}
        variant="outlined"
      />
      <TextField
        className={classes.state}
        label="State"
        id="store"
        name="state"
        onChange={handleInput}
        value={state.state || ''}
        variant="outlined"
      />
      <TextField
        className={classes.zip}
        label="Zip"
        id="store"
        name="zip"
        onChange={handleInput}
        value={state.zip || ''}
        variant="outlined"
      />
      <TextField
        className={classes.input}
        label="Point of Contact"
        id="store"
        name="pointOfContact"
        onChange={handleInput}
        value={state.pointOfContact || ''}
        variant="outlined"
        fullWidth
      />
    </React.Fragment>
  )
}

export default Store
