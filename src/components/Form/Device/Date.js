import React from 'react'
import { useEffect } from 'react'

import DateFnsUtils from '@date-io/date-fns'
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers'

const Date = () => {
  const [selectedDate, setSelectedDate] = React.useState(
    new window.Date('2014-08-18T21:11:54')
  )

  // useEffect(() => {
  //   console.log(selectedDate)
  // })

  return (
    <React.Fragment>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          disableToolbar
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          id="device"
          name="date"
          fullWidth
          label="Repair By Date"
          value={selectedDate}
          onChange={setSelectedDate}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
      </MuiPickersUtilsProvider>
    </React.Fragment>
  )
}

export default Date
