import React from 'react'
import { useEffect } from 'react'

import Date from './Date'

import {
  TextField,
  FormControlLabel,
  Checkbox,
  Typography,
  FormControl,
  InputLabel,
  Select,
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  input: {
    marginBottom: '10px',
  },
  bold: {
    fontWeight: 'bold',
  },
  subtitle: {
    marginBottom: '10px',
    fontWeight: 'bold',
  },
  description: {
    marginBottom: '10px',
  },
})

const Device = () => {
  const classes = useStyles()
  const [state, setState] = React.useState({
    type: '',
    serial: '',
    password: '',
    noPower: false,
    noBacklight: false,
    noCharge: false,
    HDMI: false,
    dockReplacement: false,
    tristar: false,
    techDamage: false,
    other: false,
    description: '',
    workorderID: 0,
  })

  // useEffect(() => {
  //   console.log(state)
  // })

  const handleInput = (e) =>
    setState({
      ...state,
      [e.target.name]: e.target.value,
    })

  const handleCheckbox = (e) =>
    setState({
      ...state,
      [e.target.name]: e.target.checked,
    })
  return (
    <React.Fragment>
      <Typography variant="h4" gutterBottom>
        Device
      </Typography>
      <FormControl variant="outlined" fullWidth>
        <InputLabel htmlFor="outlined-age-native-simple">Device</InputLabel>
        <Select
          native
          id="device"
          value={state.type}
          onChange={handleInput}
          name="type"
          label="Device"
          className={classes.input}>
          <option aria-label="None" value="" />
          <option value={'iPhone'}>iPhone</option>
          <option value={'iPad'}>iPad</option>
          <option value={'Macbook'}>Macbook</option>
          <option value={'Macbook'}>Nintendo Switch</option>
          <option value={'Macbook'}>PS4</option>
          <option value={'Macbook'}>Xbox One</option>
        </Select>
        <TextField
          className={classes.input}
          label="Serial Number/IMEI"
          id="device"
          name="serial"
          onChange={handleInput}
          value={state.serial || ''}
          variant="outlined"
        />
        <TextField
          className={classes.input}
          label="Password"
          id="device"
          name="password"
          onChange={handleInput}
          value={state.password || ''}
          variant="outlined"
        />
      </FormControl>
      <Typography variant="subtitle1" className={classes.bold}>
        What type of repair is this?
      </Typography>

      <FormControlLabel
        control={
          <Checkbox
            id="device"
            checked={state.noPower || false}
            onChange={handleCheckbox}
            name="noPower"
            color="primary"
          />
        }
        label="No Power"
      />
      <FormControlLabel
        control={
          <Checkbox
            id="device"
            checked={state.noBacklight || false}
            onChange={handleCheckbox}
            name="noBacklight"
            color="primary"
          />
        }
        label="No Backlight"
      />
      <FormControlLabel
        control={
          <Checkbox
            id="device"
            checked={state.noCharge || false}
            onChange={handleCheckbox}
            name="noCharge"
            color="primary"
          />
        }
        label="Charging Issue"
      />
      <FormControlLabel
        control={
          <Checkbox
            id="device"
            checked={state.HDMI || false}
            onChange={handleCheckbox}
            name="HDMI"
            color="primary"
          />
        }
        label="Console HDMI Port"
      />
      <FormControlLabel
        control={
          <Checkbox
            id="device"
            checked={state.dockReplacement || false}
            onChange={handleCheckbox}
            name="dockReplacement"
            color="primary"
          />
        }
        label="iPad Dock"
      />
      <FormControlLabel
        control={
          <Checkbox
            id="device"
            checked={state.tristar || false}
            onChange={handleCheckbox}
            name="tristar"
            color="primary"
          />
        }
        label="Tri-star"
      />
      <FormControlLabel
        control={
          <Checkbox
            id="device"
            checked={state.techDamage || false}
            onChange={handleCheckbox}
            name="techDamage"
            color="primary"
          />
        }
        label="Visible Tech Damage"
      />
      <FormControlLabel
        control={
          <Checkbox
            id="device"
            checked={state.other || false}
            onChange={handleCheckbox}
            name="other"
            color="primary"
          />
        }
        label="Other"
      />

      <Typography className={classes.subtitle} variant="subtitle1" gutterBottom>
        What is wrong with this device?
      </Typography>
      <TextField
        className={classes.description}
        fullWidth
        id="device"
        multiline
        rows={3}
        name="description"
        value={state.description || ''}
        onChange={handleInput}
        variant="outlined"
      />
      <Typography className={classes.subtitle} variant="subtitle1" gutterBottom>
        Please scan the workorder label.
      </Typography>
      <TextField
        className={classes.input}
        label="WO #"
        id="device"
        name="workorderID"
        fullWidth
        onChange={handleInput}
        value={state.workorderID || ''}
        variant="outlined"
      />

      <Date />
    </React.Fragment>
  )
}

export default Device
