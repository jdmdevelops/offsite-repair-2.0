import React from 'react'
import { useEffect } from 'react'

import { FormControlLabel, Checkbox, Typography } from '@material-ui/core'

const CheckList = () => {
  const [state, setState] = React.useState({
    checkedA: false,
    checkedB: false,
  })

  // useEffect(() => {
  //   console.log(state)
  // })

  const handleCheckbox = (e) =>
    setState({
      ...state,
      [e.target.name]: e.target.checked,
    })

  return (
    <React.Fragment>
      <Typography variant="h4">Checklist</Typography>
      <FormControlLabel
        control={
          <Checkbox
            id="checklist"
            checked={state.checkedA || false}
            onChange={handleCheckbox}
            name="checkedA"
            color="primary"
          />
        }
        label="I have confirmed that this device does not have liquid damage."
      />
      <FormControlLabel
        control={
          <Checkbox
            id="checklist"
            checked={state.checkedB || false}
            onChange={handleCheckbox}
            name="checkedB"
            color="primary"
          />
        }
        label="I have contacted the Fredericksburg store (540-371-3349) and spoke to either Justin or Colin about this device."
      />
    </React.Fragment>
  )
}

export default CheckList
