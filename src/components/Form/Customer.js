import React from 'react'
import { useEffect } from 'react'

import { TextField, Typography } from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  input: {
    marginBottom: '10px',
  },
})

const Customer = () => {
  const classes = useStyles()
  const [state, setState] = React.useState({
    name: '',
    phoneNumber: '',
  })

  const handleInput = (e) =>
    setState({
      ...state,
      [e.target.name]: e.target.value,
    })

  return (
    <React.Fragment>
      <Typography variant="h4" gutterBottom>
        Customer
      </Typography>
      <TextField
        className={classes.input}
        label="Name"
        fullWidth
        id="customer"
        name="name"
        onChange={handleInput}
        value={state.name || ''}
        variant="outlined"
      />
      <TextField
        className={classes.input}
        label="Phone #"
        fullWidth
        id="customer"
        name="phoneNumber"
        onChange={handleInput}
        value={state.phoneNumber || ''}
        variant="outlined"
      />
    </React.Fragment>
  )
}

export default Customer
