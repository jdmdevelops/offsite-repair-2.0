import React from 'react'
import { useEffect } from 'react'

import CheckList from './Checklist'
import Device from './Device/Device'
import Customer from './Customer'
import Store from './Store'

import { Container } from '@material-ui/core'

const Form = () => {
  return (
    <div>
      <Container maxWidth="sm">
        <form className="form" noValidate autoComplete="off">
          <CheckList />
          <Device />
          <Customer />
          <Store />
        </form>
      </Container>
    </div>
  )
}

export default Form
